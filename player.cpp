#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    bd = new Board();
    sd = side;
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {    
    Side osd;
    if (sd == BLACK)
    {
        osd = WHITE;
    }
    else
    {
        osd = BLACK;
    }
    bd->doMove(opponentsMove, osd);     // Process opponent's move.
    
    Move *move = new Move(0, 0);
    Move *maxmove = new Move(0, 0);
    int maxcount = -64;
    
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)     // Iterate through possible moves.
        {
            move->setX(i);
            move->setY(j);
            if (bd->checkMove(move, sd))
            {
                Board *bd1 = bd->copy();
                bd1->doMove(move, sd);
                
                int mincount = 64;
                
                mincount = doStep(bd1, osd, sd, 4);
                // Calculate four more moves, for 5 total moves.
                delete bd1;
                if (mincount > maxcount)    // minimax move
                {
                    maxmove->setX(i);
                    maxmove->setY(j);
                    maxcount = mincount;
                }
            }
        }
    }
    delete move;
    
    if (maxcount == -64) // No valid moves found.
    {
        return NULL;
    }
    
    bd->doMove(maxmove, sd);
    return maxmove;
}

/* Perform a step and return the minimum. */
int Player::doStep(Board *board, Side sd, Side osd, int left)
{
    Move *tmove = new Move(0, 0);
    int mincount = 64;
    int newcount;
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            tmove->setX(i);
            tmove->setY(j);
            if (board->checkMove(tmove, sd))
            {   // Move is valid.
                Board *tboard = board->copy();
                tboard->doMove(tmove, sd);
                if (left == 0)
                {   // Last step.
                    newcount = tboard->countMore(osd);
                }
                else
                {   // Perform another step.
                    newcount = doStep(tboard, osd, sd, left - 1);
                }
                if (newcount < mincount)
                {
                    mincount = newcount;
                }   // Keep and return minimum final value.
                delete tboard;
            }
        }
    }
    return mincount;
}



/* Set a player's board. */
void Player::setBoard(Board *board) {
    bd = board;
}
