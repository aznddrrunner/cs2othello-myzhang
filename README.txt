I worked solo on this project.

I improved the AI by calculating 5 total moves in advance. I did this by
writing a function which recursively calls itself on all possible next
moves, and has a counter to stop recursively calling on the 5th move.
This function can be called much more than 5 times in order to make the
AI even better. However, improvements in time were not implemented, so
there is not enough time to view more than 5 moves in advance. Other
improvements that were attempted were alpha-beta pruning and ranking
certain tiles over others, but these did not work so they were not
implemented.
