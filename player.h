#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

private:
    Board *bd;
    Side sd;
public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    int doStep(Board *board, Side sd, Side osd, int left);
    void setBoard(Board *board);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;

};

#endif
